# Tovarishch Xi Bot

Telegram bot for management groups

## Whan can it do?

Each user in the group has his own number of medals and warnings. The group administrator can give each user a count of medals and warnings. All users can see the statistics of medals and warnings of users

## How to use?

1. To give out medals, the administrator must reply to the user with a message, which in the beginning contains a certain number of pluses. For example, *+++* - user will receive 3 medals. *+* - 1 medal. One message can give up to 5 medals, that is, if you send a message with the text *++++++++++*, the user will receive only 5 medals instead of 10. If you don't want to reply to the user's message, you may indicate his user name after the pluses; be sure to put an *@* sign before the user name, for example *+++ @coolvovan2003*.
2. To issue warnings, the administrator must reply to the user with a message with the text ! If you don't want to reply to the message, you can specify a user name after !warn; be sure to put an *@* sign before the user name, for example *!warn @coolvovan2003*.
3. If the administrator wants to reset the user data, he can do it with the *!refresh* command. Answer the user's message or follow !refresh with the user name, making sure to put @ in front of the user name, e.g. *!refresh @coolvovan2003*.
4. All users can invoke the /stats command. This command sends a message with information about who has the most medals, who has the least medals, who has the most warnings, who has the least warnings.

## Environment variables

1. **DB_URL** - required, *jdbc:mariadb//**HOST**:**PORT**/**DB_NAME**?user=**USERNAME**&password=**PASSWORD***
2. **BOT_TOKEN** - required, get from Botfather
3. **BOT_USERNAME** - required

## Run

```bash
cd TO_PROJECT_DIRECTORY
docker build -t tovarishch-xi-bot:0.0.1 .
docker run --name tovarishch-xi-bot tovarishch-xi-bot:0.0.1
```

