package io.github.t3m8ch.tovarishchxijdbc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TovarishchXiJdbcApplication

fun main(args: Array<String>) {
    runApplication<TovarishchXiJdbcApplication>(*args)
}
