package io.github.t3m8ch.tovarishchxijdbc.services

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO

interface UsersService {
    fun updateOrCreate(dto: CreateUpdateUserDTO): UserModel
}
