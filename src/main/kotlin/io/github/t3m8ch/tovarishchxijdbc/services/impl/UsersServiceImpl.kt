package io.github.t3m8ch.tovarishchxijdbc.services.impl

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.services.UsersService
import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxijdbc.services.utils.mapToModel
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class UsersServiceImpl(private val usersRepository: UsersRepository) : UsersService {
    @Transactional
    override fun updateOrCreate(dto: CreateUpdateUserDTO): UserModel {
        val existingUser = usersRepository.findOrNullByTelegramId(dto.telegramId)
        if (existingUser != null) {
            return updateExistingUser(existingUser, dto)
        }
        return usersRepository.save(dto.mapToModel())
    }

    private fun updateExistingUser(existingUser: UserModel, dto: CreateUpdateUserDTO): UserModel {
        if (valuesHaveNotChanged(existingUser, dto)) {
            return existingUser
        }

        existingUser.firstName = dto.firstName
        existingUser.lastName = dto.lastName
        existingUser.userName = dto.userName
        existingUser.updatedAt = Instant.now()

        return usersRepository.save(existingUser)
    }

    private fun valuesHaveNotChanged(existingUser: UserModel, dto: CreateUpdateUserDTO) =
        existingUser.firstName == dto.firstName &&
                existingUser.lastName == dto.lastName &&
                existingUser.userName == dto.userName
}
