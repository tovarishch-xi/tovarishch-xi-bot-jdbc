package io.github.t3m8ch.tovarishchxijdbc.services

typealias NewMedalsCount = Int

interface MedalsService {
    fun giveMedalsByTelegramId(count: Int, targetTelegramId: Long): NewMedalsCount

    fun giveMedalsByUserName(count: Int, targetUserName: String): NewMedalsCount
}
