package io.github.t3m8ch.tovarishchxijdbc.services.utils

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO

fun CreateUpdateUserDTO.mapToModel() = UserModel(
    telegramId = telegramId,
    firstName = firstName,
    lastName = lastName,
    userName = userName,
)
