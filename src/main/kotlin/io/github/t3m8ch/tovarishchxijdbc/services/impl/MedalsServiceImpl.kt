package io.github.t3m8ch.tovarishchxijdbc.services.impl

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import io.github.t3m8ch.tovarishchxijdbc.services.NewMedalsCount
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class MedalsServiceImpl(private val usersRepository: UsersRepository) : MedalsService {
    @Transactional
    override fun giveMedalsByTelegramId(count: Int, targetTelegramId: Long): NewMedalsCount {
        val user = usersRepository.findOrNullByTelegramId(targetTelegramId)
            ?: throw UserWithThisTelegramIdNotFoundException(targetTelegramId)
        return giveMedals(count, user)
    }

    @Transactional
    override fun giveMedalsByUserName(count: Int, targetUserName: String): NewMedalsCount {
        val user = usersRepository.findOrNullByUserName(targetUserName)
            ?: throw UserWithThisUserNameNotFoundException(targetUserName)
        return giveMedals(count, user)
    }

    private fun giveMedals(count: Int, user: UserModel): NewMedalsCount {
        user.medalsCount += count
        user.updatedAt = Instant.now()
        return usersRepository.save(user).medalsCount
    }
}
