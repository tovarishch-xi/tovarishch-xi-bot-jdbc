package io.github.t3m8ch.tovarishchxijdbc.services

typealias NewWarnsCount = Int

interface WarnsService {
    fun warnByUserName(targetUserName: String): NewWarnsCount
    fun warnByTelegramId(targetTelegramId: Long): NewWarnsCount
}
