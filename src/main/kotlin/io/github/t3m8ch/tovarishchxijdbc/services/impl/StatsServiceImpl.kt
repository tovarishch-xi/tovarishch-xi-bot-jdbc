package io.github.t3m8ch.tovarishchxijdbc.services.impl

import io.github.t3m8ch.tovarishchxijdbc.MAX_COUNT_OF_USERS_IN_STATS
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.services.StatsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.TextRenderer
import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.factories.StatsTextRendererFactory
import org.springframework.stereotype.Service

@Service
class StatsServiceImpl(
    private val usersRepository: UsersRepository,
    private val statsTextRendererFactory: StatsTextRendererFactory,
) : StatsService {
    override fun getStats(): TextRenderer {
        val users = usersRepository.findAll()

        val topOfUsersByMedals = users
            .filter { it.medalsCount > 0 }
            .sortedByDescending { it.medalsCount }
            .take(MAX_COUNT_OF_USERS_IN_STATS)

        val antiTopOfUsersByMedals = users
            .filter { it.medalsCount > 0 }
            .sortedBy { it.medalsCount }
            .take(MAX_COUNT_OF_USERS_IN_STATS)

        val topOfUsersByWarns = users
            .filter { it.warnsCount > 0 }
            .sortedByDescending { it.warnsCount }
            .take(MAX_COUNT_OF_USERS_IN_STATS)

        val antiTopOfUsersByWarns = users
            .filter { it.warnsCount > 0 }
            .sortedBy { it.warnsCount }
            .take(MAX_COUNT_OF_USERS_IN_STATS)

        return statsTextRendererFactory.create(
            topOfUsersByMedals,
            antiTopOfUsersByMedals,
            topOfUsersByWarns,
            antiTopOfUsersByWarns,
        )
    }
}
