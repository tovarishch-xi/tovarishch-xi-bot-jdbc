package io.github.t3m8ch.tovarishchxijdbc.services.dto

data class CreateUpdateUserDTO(
    val telegramId: Long,
    val firstName: String,
    val lastName: String? = null,
    val userName: String? = null,
) {
    companion object
}
