package io.github.t3m8ch.tovarishchxijdbc.services.impl

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.services.NewWarnsCount
import io.github.t3m8ch.tovarishchxijdbc.services.WarnsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class WarnsServiceImpl(private val usersRepository: UsersRepository) : WarnsService {
    @Transactional
    override fun warnByUserName(targetUserName: String): NewWarnsCount {
        val user = usersRepository.findOrNullByUserName(targetUserName)
            ?: throw UserWithThisUserNameNotFoundException(targetUserName)
        return warn(user)
    }

    @Transactional
    override fun warnByTelegramId(targetTelegramId: Long): NewWarnsCount {
        val user = usersRepository.findOrNullByTelegramId(targetTelegramId)
            ?: throw UserWithThisTelegramIdNotFoundException(targetTelegramId)
        return warn(user)
    }

    private fun warn(user: UserModel): NewWarnsCount {
        user.warnsCount++
        user.updatedAt = Instant.now()
        return usersRepository.save(user).warnsCount
    }
}
