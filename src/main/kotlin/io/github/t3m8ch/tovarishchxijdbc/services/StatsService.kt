package io.github.t3m8ch.tovarishchxijdbc.services

import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.TextRenderer

interface StatsService {
    fun getStats(): TextRenderer
}
