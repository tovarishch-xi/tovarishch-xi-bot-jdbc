package io.github.t3m8ch.tovarishchxijdbc.services

interface RefreshService {
    fun refreshByUserName(targetUserName: String)
    fun refreshByTelegramId(targetTelegramId: Long)
}
