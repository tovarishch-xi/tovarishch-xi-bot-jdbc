package io.github.t3m8ch.tovarishchxijdbc.services.impl

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.services.RefreshService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class RefreshServiceImpl(private val usersRepository: UsersRepository) : RefreshService {
    @Transactional
    override fun refreshByUserName(targetUserName: String) {
        val user = usersRepository.findOrNullByUserName(targetUserName)
            ?: throw UserWithThisUserNameNotFoundException(targetUserName)
        refresh(user)
    }

    @Transactional
    override fun refreshByTelegramId(targetTelegramId: Long) {
        val user = usersRepository.findOrNullByTelegramId(targetTelegramId)
            ?: throw UserWithThisTelegramIdNotFoundException(targetTelegramId)
        refresh(user)
    }

    private fun refresh(user: UserModel) {
        user.medalsCount = 0
        user.warnsCount = 0
        usersRepository.save(user)
    }
}
