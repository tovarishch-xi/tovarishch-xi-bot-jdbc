package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import io.github.t3m8ch.tovarishchxijdbc.services.NewMedalsCount
import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.isGroupMessage
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.senderIsAdmin
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.getCharacterCountInStringInRow
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.getUserNameFromMessageTextOrNull
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.replyToBot
import org.springframework.stereotype.Component
import org.telegram.abilitybots.api.util.AbilityUtils.getUser
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import kotlin.math.min

const val MAX_COUNT_OF_MEDALS = 5

@Component
class GiveMedalsCommandHandler(private val medalsService: MedalsService) : Handler({
    val startsWithPlus = it.update.message?.text?.startsWith("+") ?: false
    startsWithPlus && isGroupMessage(it) && senderIsAdmin(it)
}) {
    override fun handle(context: Context) {
        if (replyToBot(context)) return sendUserIsBotMessage(context)

        val medalsCount = getMedalsCount(context)
        val newMedalsCount = giveMedals(medalsCount, context) ?: return

        sendSuccessfulGivingMedalsMessage(context, medalsCount, newMedalsCount)
    }

    private fun getMedalsCount(context: Context) = min(
        context.update.message.text.getCharacterCountInStringInRow('+'),
        MAX_COUNT_OF_MEDALS
    )

    private fun giveMedals(medalsCount: Int, context: Context): NewMedalsCount? {
        val targetUserName = getUserNameFromMessageTextOrNull(context)
        val targetTelegramId = context.update.message.replyToMessage?.from?.id

        return when {
            targetUserName != null -> {
                if (userAddedMedalsToHimself(targetUserName, context)) return null
                medalsService.giveMedalsByUserName(medalsCount, targetUserName)
            }
            targetTelegramId != null -> {
                if (userAddedMedalsToHimself(targetTelegramId, context)) return null
                medalsService.giveMedalsByTelegramId(medalsCount, targetTelegramId)
            }
            else -> {
                null
            }
        }
    }

    private fun userAddedMedalsToHimself(targetUserName: String, context: Context) =
        targetUserName == getUser(context.update).userName

    private fun userAddedMedalsToHimself(targetTelegramId: Long, context: Context) =
        targetTelegramId == getUser(context.update).id

    private fun sendUserIsBotMessage(context: Context) {
        val method = SendMessage.builder()
            .text("Вы не можете выдавать медальки боту")
            .replyToMessageId(context.update.message.messageId)
            .chatId(context.update.message.chatId)
            .build()

        context.sender.execute(method)
    }

    private fun sendSuccessfulGivingMedalsMessage(context: Context, medalsCount: Int, newTotalMedalsCount: Int) {
        val method = SendMessage.builder()
            .replyToMessageId(context.update.message.messageId)
            .text(
                """
                    Вы выдали медальки в кол-ве $medalsCount шт.
                    Теперь у этого пользователя медалек $newTotalMedalsCount шт.
                """.trimIndent()
            )
            .chatId(context.update.message.chatId)
            .build()

        context.sender.execute(method)
    }
}
