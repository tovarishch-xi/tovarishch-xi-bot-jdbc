package io.github.t3m8ch.tovarishchxijdbc.telegram.renderers

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel

class StatsTextRenderer(
    private val topOfUsersByMedals: List<UserModel>,
    private val antiTopOfUsersByMedals: List<UserModel>,
    private val topOfUsersByWarns: List<UserModel>,
    private val antiTopOfUsersByWarns: List<UserModel>,
) : TextRenderer {
    override fun render(): String {
        val stringBuilder = StringBuilder()

        appendTopOfUsersByMedalsCount(stringBuilder)
        stringBuilder.appendLine()
        appendAntiTopOfUsersByMedalsCount(stringBuilder)
        stringBuilder.appendLine()
        appendTopOfUsersByWarnsCount(stringBuilder)
        stringBuilder.appendLine()
        appendAntiTopOfUsersByWarnsCount(stringBuilder)
        stringBuilder.appendLine()
        stringBuilder.append("<i>*Пользователи, у кого нет медалей, не выводятся</i>\n")
        stringBuilder.append("<i>**Пользователи, у кого нет предупреждений, не выводятся</i>")

        return stringBuilder.toString()
    }

    private fun appendTopOfUsersByMedalsCount(stringBuilder: StringBuilder) {
        stringBuilder.appendLine("🥇 Топ пользователей по кол-ву медалей*:")
        topOfUsersByMedals.forEachIndexed { i, user ->
            stringBuilder.appendLine(renderUserLineWithMedalsCount(user, i + 1))
        }
    }

    private fun appendAntiTopOfUsersByMedalsCount(stringBuilder: StringBuilder) {
        stringBuilder.appendLine("У кого меньше всего медалей*:")
        antiTopOfUsersByMedals.forEachIndexed { i, user ->
            stringBuilder.appendLine(renderUserLineWithMedalsCount(user, i + 1))
        }
    }

    private fun appendTopOfUsersByWarnsCount(stringBuilder: StringBuilder) {
        stringBuilder.appendLine("⸸ Топ пользователей по кол-ву предупреждений**:")
        topOfUsersByWarns.forEachIndexed { i, user ->
            stringBuilder.appendLine(renderUserLineWithWarnsCount(user, i + 1))
        }
    }

    private fun appendAntiTopOfUsersByWarnsCount(stringBuilder: StringBuilder) {
        stringBuilder.appendLine("У кого меньше всего предупреждений**:")
        antiTopOfUsersByWarns.forEachIndexed { i, user ->
            stringBuilder.appendLine(renderUserLineWithWarnsCount(user, i + 1))
        }
    }

    private fun renderUserLineWithMedalsCount(user: UserModel, i: Int) =
        "$i. ${renderName(user)} - ${user.medalsCount}"

    private fun renderUserLineWithWarnsCount(user: UserModel, i: Int) =
        "$i. ${renderName(user)} - ${user.warnsCount}"

    private fun renderName(user: UserModel): String {
        val piecesOfName = mutableListOf(user.firstName)
        if (user.lastName != null) piecesOfName.add(user.lastName!!)
        if (user.userName != null) piecesOfName.add("(@${user.userName!!})")

        return piecesOfName.joinToString(separator = " ")
    }
}
