package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context

abstract class Handler(val filter: (Context) -> Boolean) {
    abstract fun handle(context: Context)
}
