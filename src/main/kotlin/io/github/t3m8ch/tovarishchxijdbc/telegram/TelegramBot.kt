package io.github.t3m8ch.tovarishchxijdbc.telegram

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.services.UsersService
import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.Handler
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.objects.Update

@Component
class TelegramBot(
    private val props: TelegramBotProperties,
    private val handlers: List<Handler>,
    private val usersService: UsersService,
): TelegramLongPollingBot() {
    override fun getBotToken() = props.token
    override fun getBotUsername() = props.username

    override fun onUpdateReceived(update: Update) {
        val user = usersService.updateOrCreate(CreateUpdateUserDTO.fromTelegramUpdate(update))
        val context = createContextFromUpdate(update, user)
        handle(context)
    }

    private fun handle(context: Context) {
        val handler = handlers.firstOrNull { it.filter(context) } ?: return
        handler.handle(context)
    }

    private fun createContextFromUpdate(update: Update, user: UserModel) = Context(
        update = update,
        sender = this,
        user = user,
    )
}
