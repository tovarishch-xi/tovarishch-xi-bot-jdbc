package io.github.t3m8ch.tovarishchxijdbc.telegram.filters

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import org.telegram.abilitybots.api.util.AbilityUtils
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMemberAdministrator
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMemberOwner

fun senderIsAdmin(context: Context): Boolean {
    val chatId = context.update.message.chatId
    val userId = AbilityUtils.getUser(context.update).id

    val method = GetChatMember.builder().chatId(chatId).userId(userId).build()

    val member = context.sender.execute(method)
    return member is ChatMemberAdministrator || member is ChatMemberOwner
}
