package io.github.t3m8ch.tovarishchxijdbc.telegram.utils

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context

fun replyToBot(context: Context) = context.update.message.replyToMessage?.from?.isBot == true
