package io.github.t3m8ch.tovarishchxijdbc.telegram

import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO
import org.telegram.abilitybots.api.util.AbilityUtils.getUser
import org.telegram.telegrambots.meta.api.objects.Update

fun CreateUpdateUserDTO.Companion.fromTelegramUpdate(update: Update): CreateUpdateUserDTO {
    val telegramUser = getUser(update)
    return CreateUpdateUserDTO(
        telegramId = telegramUser.id,
        firstName = telegramUser.firstName,
        lastName = telegramUser.lastName,
        userName = telegramUser.userName,
    )
}
