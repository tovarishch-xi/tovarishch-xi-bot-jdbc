package io.github.t3m8ch.tovarishchxijdbc.telegram.utils

fun String.getCharacterCountInStringInRow(char: Char): Int {
    var count = 0
    for (currentChar in this) {
        if (currentChar != char) break
        count++
    }
    return count
}
