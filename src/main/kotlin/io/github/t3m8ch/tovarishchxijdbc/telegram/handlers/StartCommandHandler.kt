package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import org.springframework.stereotype.Component
import org.telegram.abilitybots.api.util.AbilityUtils.getChatId
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
class StartCommandHandler : Handler({ it.update.message?.text == "/start" }) {
    override fun handle(context: Context) {
        val method = SendMessage.builder()
            .text("Привет! Я Товарищ Хи!")
            .chatId(getChatId(context.update))
            .build()

        context.sender.execute(method)
    }
}
