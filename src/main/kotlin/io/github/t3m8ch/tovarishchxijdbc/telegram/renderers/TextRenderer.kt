package io.github.t3m8ch.tovarishchxijdbc.telegram.renderers

interface TextRenderer {
    fun render(): String
}
