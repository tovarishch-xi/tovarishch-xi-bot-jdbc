package io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.factories

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.StatsTextRenderer
import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.TextRenderer
import org.springframework.stereotype.Component

interface StatsTextRendererFactory {
    fun create(
        topOfUsersByMedals: List<UserModel>,
        antiTopOfUsersByMedals: List<UserModel>,
        topOfUsersByWarns: List<UserModel>,
        antiTopOfUsersByWarns: List<UserModel>
    ): TextRenderer
}

@Component
class StatsTextRendererFactoryImpl : StatsTextRendererFactory {
    override fun create(
        topOfUsersByMedals: List<UserModel>,
        antiTopOfUsersByMedals: List<UserModel>,
        topOfUsersByWarns: List<UserModel>,
        antiTopOfUsersByWarns: List<UserModel>
    ) = StatsTextRenderer(topOfUsersByMedals, antiTopOfUsersByMedals, topOfUsersByWarns, antiTopOfUsersByWarns)
}
