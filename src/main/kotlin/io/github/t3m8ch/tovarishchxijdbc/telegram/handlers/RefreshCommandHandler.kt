package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.services.RefreshService
import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.isGroupMessage
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.senderIsAdmin
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.getUserNameFromMessageTextOrNull
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.replyToBot
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
class RefreshCommandHandler(private val refreshService: RefreshService) : Handler({
    it.update.message?.text?.startsWith("!refresh") == true && isGroupMessage(it) && senderIsAdmin(it)
}) {
    override fun handle(context: Context) {
        if (replyToBot(context)) return sendUserIsBotMessage(context)

        val targetUserName = getUserNameFromMessageTextOrNull(context)
        val targetTelegramId = context.update.message.replyToMessage?.from?.id

        when {
            targetUserName != null -> {
                refreshService.refreshByUserName(targetUserName)
            }
            targetTelegramId != null -> {
                refreshService.refreshByTelegramId(targetTelegramId)
            }
            else -> {
                sendErrorMessageToUnspecifiedUser(context)
                return
            }
        }

        sendSuccessfulRefreshMessage(context)
    }

    private fun sendUserIsBotMessage(context: Context) {
        val method = SendMessage.builder()
            .text("Вы не можете обнулить медали и предупреждения бота")
            .replyToMessageId(context.update.message.messageId)
            .chatId(context.update.message.chatId)
            .build()

        context.sender.execute(method)
    }

    private fun sendSuccessfulRefreshMessage(context: Context) {
        context.sender.execute(
            SendMessage.builder()
                .text("Данные пользователя обнулены")
                .replyToMessageId(context.update.message.messageId)
                .chatId(context.update.message.chatId)
                .build()
        )
    }

    private fun sendErrorMessageToUnspecifiedUser(context: Context) {
        context.sender.execute(
            SendMessage.builder()
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!refresh @vasya2005</code>
                    """.trimIndent()
                )
                .replyToMessageId(context.update.message.messageId)
                .chatId(context.update.message.chatId)
                .parseMode("HTML")
                .build()
        )
    }
}
