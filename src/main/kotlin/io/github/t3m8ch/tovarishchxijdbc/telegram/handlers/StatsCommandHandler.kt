package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.services.StatsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
class StatsCommandHandler(private val statsService: StatsService) : Handler({ it.update.message?.text == "/stats" }) {
    override fun handle(context: Context) {
        context.sender.execute(
            SendMessage.builder()
                .text(statsService.getStats().render())
                .replyToMessageId(context.update.message.messageId)
                .chatId(context.update.message.chatId)
                .parseMode("HTML")
                .build()
        )
    }
}
