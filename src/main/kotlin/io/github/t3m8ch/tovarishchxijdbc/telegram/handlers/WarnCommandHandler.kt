package io.github.t3m8ch.tovarishchxijdbc.telegram.handlers

import io.github.t3m8ch.tovarishchxijdbc.services.NewWarnsCount
import io.github.t3m8ch.tovarishchxijdbc.services.WarnsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.isGroupMessage
import io.github.t3m8ch.tovarishchxijdbc.telegram.filters.senderIsAdmin
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.getUserNameFromMessageTextOrNull
import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.replyToBot
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
class WarnCommandHandler(private val warnsService: WarnsService) : Handler({
    it.update.message?.text?.startsWith("!warn") == true && isGroupMessage(it) && senderIsAdmin(it)
}) {
    override fun handle(context: Context) {
        if (replyToBot(context)) return sendUserIsBotMessage(context)

        val targetUserName = getUserNameFromMessageTextOrNull(context)
        val targetTelegramId = context.update.message.replyToMessage?.from?.id

        val newWarnsCount = when {
            targetUserName != null -> {
                warnsService.warnByUserName(targetUserName)
            }
            targetTelegramId != null -> {
                warnsService.warnByTelegramId(targetTelegramId)
            }
            else -> {
                sendErrorMessageToUnspecifiedUser(context)
                return
            }
        }

        sendSuccessfulGivingWarnMessage(newWarnsCount, context)
    }

    private fun sendUserIsBotMessage(context: Context) {
        val method = SendMessage.builder()
            .text("Вы не можете выдавать предупреждения боту")
            .replyToMessageId(context.update.message.messageId)
            .chatId(context.update.message.chatId)
            .build()

        context.sender.execute(method)
    }

    private fun sendSuccessfulGivingWarnMessage(newWarnsCount: NewWarnsCount, context: Context) {
        context.sender.execute(
            SendMessage.builder()
                .text(
                    """
                            Вы выдали предупреждение пользователю.
                            Теперь у этого пользователя $newWarnsCount предупреждений
                        """.trimIndent()
                )
                .replyToMessageId(context.update.message.messageId)
                .chatId(context.update.message.chatId)
                .build()
        )
    }

    private fun sendErrorMessageToUnspecifiedUser(context: Context) {
        context.sender.execute(
            SendMessage.builder()
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!warn @vasya2005</code>
                    """.trimIndent()
                )
                .replyToMessageId(context.update.message.messageId)
                .chatId(context.update.message.chatId)
                .parseMode("HTML")
                .build()
        )
    }
}
