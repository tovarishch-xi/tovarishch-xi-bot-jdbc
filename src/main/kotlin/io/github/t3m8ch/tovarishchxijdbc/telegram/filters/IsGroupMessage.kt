package io.github.t3m8ch.tovarishchxijdbc.telegram.filters

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context

fun isGroupMessage(context: Context) =
    context.update.message.isGroupMessage || context.update.message.isSuperGroupMessage

