package io.github.t3m8ch.tovarishchxijdbc.telegram

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.bots.AbsSender

data class Context(
    val update: Update,
    val sender: AbsSender,
    val user: UserModel,
)
