package io.github.t3m8ch.tovarishchxijdbc.telegram.utils

import io.github.t3m8ch.tovarishchxijdbc.telegram.Context

fun getUserNameFromMessageTextOrNull(context: Context): String? {
    val splitText = context.update.message.text.split(" ")
    return if (splitText.getOrNull(1)?.startsWith("@") == true) {
        splitText.getOrNull(1)?.filterIndexed { i, _ -> i > 0 }
    } else {
        null
    }
}
