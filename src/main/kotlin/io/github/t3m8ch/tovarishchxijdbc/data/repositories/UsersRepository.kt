package io.github.t3m8ch.tovarishchxijdbc.data.repositories

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import org.springframework.data.repository.CrudRepository

interface UsersRepository : CrudRepository<UserModel, Long> {
    fun findOrNullByTelegramId(telegramId: Long): UserModel?
    fun findOrNullByUserName(targetUserName: String): UserModel?
}
