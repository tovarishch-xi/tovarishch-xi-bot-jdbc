package io.github.t3m8ch.tovarishchxijdbc.data.models

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("user_account")
data class UserModel(
    @Id var id: Long? = null,
    @Column("telegram_id") val telegramId: Long,
    @Column("created_at") val createdAt: Instant = Instant.now(),
    @Column("updated_at") var updatedAt: Instant = Instant.now(),
    @Column("first_name") var firstName: String,
    @Column("last_name") var lastName: String? = null,
    @Column("user_name") var userName: String? = null,
    @Column("medals_count") var medalsCount: Int = 0,
    @Column("warns_count") var warnsCount: Int = 0,
)
