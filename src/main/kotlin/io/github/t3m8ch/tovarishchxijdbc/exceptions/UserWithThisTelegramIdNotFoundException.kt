package io.github.t3m8ch.tovarishchxijdbc.exceptions

class UserWithThisTelegramIdNotFoundException(val telegramId: Long) :
    RuntimeException("User with telegram id = $telegramId not found")
