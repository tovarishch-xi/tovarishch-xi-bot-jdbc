package io.github.t3m8ch.tovarishchxijdbc.exceptions

class UserWithThisUserNameNotFoundException(val userName: String)
    : RuntimeException("User with userName = $userName not found")
