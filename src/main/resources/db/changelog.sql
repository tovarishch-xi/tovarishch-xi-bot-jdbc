-- liquibase formatted sql

-- changeset t3m8ch:1
CREATE TABLE user_account
(
    id          BIGINT PRIMARY KEY AUTO_INCREMENT,
    telegram_id BIGINT UNIQUE NOT NULL,
    created_at  TIMESTAMP     NOT NULL,
    updated_at  TIMESTAMP,
    first_name  VARCHAR(255)  NOT NULL,
    last_name   VARCHAR(255),
    user_name   VARCHAR(255)
);

-- changeset t3m8ch:2
ALTER TABLE user_account
ADD COLUMN medals_count INT NOT NULL;

-- changeset t3m8ch:3
ALTER TABLE user_account
ADD COLUMN warns_count INT NOT NULL;
