package io.github.t3m8ch.tovarishchxijdbc

import io.github.t3m8ch.tovarishchxijdbc.telegram.TelegramBot
import io.github.t3m8ch.tovarishchxijdbc.telegram.TelegramBotProperties
import org.springframework.boot.test.mock.mockito.MockBean
import org.telegram.telegrambots.starter.TelegramBotInitializer

abstract class TestWithoutTelegramBot {
    @MockBean
    lateinit var telegramBot: TelegramBot
    @MockBean
    lateinit var telegramBotProperties: TelegramBotProperties
    @MockBean
    lateinit var telegramBotInitializer: TelegramBotInitializer
}
