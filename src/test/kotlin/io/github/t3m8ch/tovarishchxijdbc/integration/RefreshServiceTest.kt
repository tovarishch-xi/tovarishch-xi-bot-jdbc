package io.github.t3m8ch.tovarishchxijdbc.integration

import io.github.t3m8ch.tovarishchxijdbc.ContextInitializer
import io.github.t3m8ch.tovarishchxijdbc.TestWithoutTelegramBot
import io.github.t3m8ch.tovarishchxijdbc.builders.userModels
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.services.RefreshService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
@ContextConfiguration(initializers = [ContextInitializer::class])
class RefreshServiceTest(
    @Autowired private val refreshService: RefreshService,
    @Autowired private val usersRepository: UsersRepository,
) : TestWithoutTelegramBot() {
    @BeforeEach
    fun beforeEach() {
        usersRepository.deleteAll()
    }

    @Test
    fun `test refresh by telegramId`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasWarns(3)
                hasMedals(3)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasWarns(3)
                hasMedals(3)
            }
        }
        usersRepository.saveAll(users)

        // Act
        refreshService.refreshByTelegramId(1)

        // Assert
        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        Assertions.assertThat(updatedUser?.warnsCount).isEqualTo(0)
        Assertions.assertThat(notUpdatedUser?.warnsCount).isEqualTo(3)
        Assertions.assertThat(updatedUser?.medalsCount).isEqualTo(0)
        Assertions.assertThat(notUpdatedUser?.medalsCount).isEqualTo(3)
    }

    @Test
    fun `test give medals by userName`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasWarns(3)
                hasMedals(3)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasWarns(3)
                hasMedals(3)
            }
        }
        usersRepository.saveAll(users)

        // Act
        refreshService.refreshByUserName("petya")

        // Assert
        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        Assertions.assertThat(updatedUser?.warnsCount).isEqualTo(0)
        Assertions.assertThat(notUpdatedUser?.warnsCount).isEqualTo(3)
        Assertions.assertThat(updatedUser?.medalsCount).isEqualTo(0)
        Assertions.assertThat(notUpdatedUser?.medalsCount).isEqualTo(3)
    }

    @Test
    fun `test give medals by telegramId, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasWarns(3)
                hasMedals(3)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasWarns(3)
                hasMedals(3)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        Assertions.assertThatThrownBy {
            refreshService.refreshByTelegramId(3)
        }.isInstanceOf(UserWithThisTelegramIdNotFoundException::class.java)
    }

    @Test
    fun `test give medals by userName, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasWarns(3)
                hasMedals(3)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasWarns(3)
                hasMedals(3)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        Assertions.assertThatThrownBy {
            refreshService.refreshByUserName("sasha")
        }.isInstanceOf(UserWithThisUserNameNotFoundException::class.java)
    }
}
