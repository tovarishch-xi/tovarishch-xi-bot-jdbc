package io.github.t3m8ch.tovarishchxijdbc.integration

import io.github.t3m8ch.tovarishchxijdbc.ContextInitializer
import io.github.t3m8ch.tovarishchxijdbc.TestWithoutTelegramBot
import io.github.t3m8ch.tovarishchxijdbc.builders.userModels
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.services.StatsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.renderers.factories.StatsTextRendererFactory
import org.junit.jupiter.api.Test
import org.mockito.kotlin.argThat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
@ContextConfiguration(initializers = [ContextInitializer::class])
class StatsServiceTest(
    @Autowired private val usersRepository: UsersRepository,
    @Autowired private val statsService: StatsService,
) : TestWithoutTelegramBot() {
    @MockBean
    private lateinit var rendererFactory: StatsTextRendererFactory

    @Test
    fun `getStats should return StatsTextRenderer with limit on count of users in tops`() {
        // Arrange
        val users = userModels {
            (31..40).reversed().forEach {
                that {
                    hasCount(1)
                    hasMedals(it)
                    hasFirstName("Vasya")
                }
            }
            (31..40).reversed().forEach {
                that {
                    hasCount(1)
                    hasWarns(it)
                    hasFirstName("Dima")
                }
            }
            that {
                hasCount(34)
                hasMedals(25)
                hasWarns(25)
                hasFirstName("Petya")
            }
            (11..20).reversed().forEach {
                that {
                    hasCount(1)
                    hasMedals(it)
                    hasFirstName("Sanya")
                }
            }
            (11..20).reversed().forEach {
                that {
                    hasCount(1)
                    hasWarns(it)
                    hasFirstName("Misha")
                }
            }
        }
        usersRepository.saveAll(users)

        // Act
        statsService.getStats()

        // Assert
        rendererFactory.create(
            topOfUsersByMedals = argThat {
                all { u -> u.firstName == "Vasya" }
            },
            antiTopOfUsersByMedals = argThat {
                all { u -> u.firstName == "Sanya" }
            },
            topOfUsersByWarns = argThat {
                all { u -> u.firstName == "Dima" }
            },
            antiTopOfUsersByWarns = argThat {
                all { u -> u.firstName == "Misha" }
            },
        )
    }

    @Test
    fun `getStats should return StatsTextRenderer with sorted users`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasMedals(5)
                hasFirstName("Sanya")
                hasLastName("Petrov")
                hasUsername("sanyok123")
            }
            that {
                hasCount(1)
                hasMedals(3)
                hasFirstName("Ivan")
                hasLastName("Golubtsov")
            }
            that {
                hasCount(1)
                hasMedals(2)
                hasFirstName("Dmitry")
            }
            that {
                hasCount(1)
                hasMedals(1)
                hasFirstName("Viktor")
                hasUsername("viktorsuper")
            }
        }
        usersRepository.saveAll(users)

        // Act
        statsService.getStats()

        // Assert
        rendererFactory.create(
            topOfUsersByMedals = argThat {
                users.sortedByDescending { it.medalsCount } == users
            },
            antiTopOfUsersByMedals = argThat {
                users.sortedBy { it.medalsCount } == users
            },
            topOfUsersByWarns = argThat {
                users.sortedByDescending { it.warnsCount } == users
            },
            antiTopOfUsersByWarns = argThat {
                users.sortedBy { it.warnsCount } == users
            },
        )
    }
}
