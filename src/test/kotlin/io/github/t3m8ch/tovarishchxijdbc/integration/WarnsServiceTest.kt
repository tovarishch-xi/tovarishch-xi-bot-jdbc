package io.github.t3m8ch.tovarishchxijdbc.integration

import io.github.t3m8ch.tovarishchxijdbc.ContextInitializer
import io.github.t3m8ch.tovarishchxijdbc.TestWithoutTelegramBot
import io.github.t3m8ch.tovarishchxijdbc.builders.userModels
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.services.WarnsService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
@ContextConfiguration(initializers = [ContextInitializer::class])
class WarnsServiceTest(
    @Autowired private val warnsService: WarnsService,
    @Autowired private val usersRepository: UsersRepository,
) : TestWithoutTelegramBot() {
    @BeforeEach
    fun beforeEach() {
        usersRepository.deleteAll()
    }

    @Test
    fun `test give warn by telegramId`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasWarns(3)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasWarns(3)
            }
        }
        usersRepository.saveAll(users)

        // Act
        val actualNewMedalsCount = warnsService.warnByTelegramId(1)

        // Assert
        Assertions.assertThat(actualNewMedalsCount).isEqualTo(4)

        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        Assertions.assertThat(updatedUser?.warnsCount).isEqualTo(4)
        Assertions.assertThat(notUpdatedUser?.warnsCount).isEqualTo(3)
    }

    @Test
    fun `test give medals by userName`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasWarns(3)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasWarns(3)
            }
        }
        usersRepository.saveAll(users)

        // Act
        val actualNewMedalsCount = warnsService.warnByUserName("petya")

        // Assert
        Assertions.assertThat(actualNewMedalsCount).isEqualTo(4)

        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        Assertions.assertThat(updatedUser?.warnsCount).isEqualTo(4)
        Assertions.assertThat(notUpdatedUser?.warnsCount).isEqualTo(3)
    }

    @Test
    fun `test give medals by telegramId, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasWarns(3)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasWarns(3)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        Assertions.assertThatThrownBy {
            warnsService.warnByTelegramId(3)
        }.isInstanceOf(UserWithThisTelegramIdNotFoundException::class.java)
    }

    @Test
    fun `test give medals by userName, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasWarns(3)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasWarns(3)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        Assertions.assertThatThrownBy {
            warnsService.warnByUserName("sasha")
        }.isInstanceOf(UserWithThisUserNameNotFoundException::class.java)
    }
}
