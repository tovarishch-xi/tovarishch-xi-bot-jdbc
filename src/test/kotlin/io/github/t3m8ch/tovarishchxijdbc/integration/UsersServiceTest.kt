package io.github.t3m8ch.tovarishchxijdbc.integration

import io.github.t3m8ch.tovarishchxijdbc.ContextInitializer
import io.github.t3m8ch.tovarishchxijdbc.TestWithoutTelegramBot
import io.github.t3m8ch.tovarishchxijdbc.builders.userModels
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.services.UsersService
import io.github.t3m8ch.tovarishchxijdbc.services.dto.CreateUpdateUserDTO
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.temporal.ChronoUnit.SECONDS

@SpringBootTest
@Transactional
@ContextConfiguration(initializers = [ContextInitializer::class])
class UsersServiceTest(
    @Autowired private val usersService: UsersService,
    @Autowired private val usersRepository: UsersRepository,
) : TestWithoutTelegramBot() {
    @BeforeEach
    fun beforeEach() {
        usersRepository.deleteAll()
    }

    @Test
    fun `test updateOrCreate(), if user does not exist`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
            }
        }
        usersRepository.saveAll(users)

        // Act
        val actualUser = usersService.updateOrCreate(CreateUpdateUserDTO(telegramId = 3, firstName = ""))

        // Assert
        assertThat(usersRepository.count()).isEqualTo(3)
        assertThat(actualUser.telegramId).isEqualTo(3)
        assertThat(actualUser.createdAt.truncatedTo(SECONDS)).isEqualTo(actualUser.updatedAt.truncatedTo(SECONDS))
    }

    @Test
    fun `test updateOrCreate(), if user exists`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
            }
        }
        usersRepository.saveAll(users)
        Thread.sleep(3000)

        // Act
        val actualUser = usersService.updateOrCreate(CreateUpdateUserDTO(telegramId = 1, firstName = ""))

        // Assert
        assertThat(actualUser.id).isEqualTo(users[0].id)
        assertThat(actualUser.telegramId).isEqualTo(users[0].telegramId)
        assertThat(actualUser.createdAt.truncatedTo(SECONDS)).isEqualTo(actualUser.updatedAt.truncatedTo(SECONDS))
    }

    @Test
    fun `test updateOfCreate(), if user exists, but user data has been updated`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
            }
        }
        usersRepository.saveAll(users)
        Thread.sleep(3000)

        // Act
        usersService.updateOrCreate(
            CreateUpdateUserDTO(
                telegramId = 2,
                firstName = "Petya",
                lastName = "Ivanov",
                userName = "coolpetr2005"
            )
        )

        // Assert
        val updatedUser = usersRepository.findOrNullByTelegramId(2)
        assertThat(updatedUser?.telegramId).isEqualTo(2)
        assertThat(updatedUser?.firstName).isEqualTo("Petya")
        assertThat(updatedUser?.lastName).isEqualTo("Ivanov")
        assertThat(updatedUser?.userName).isEqualTo("coolpetr2005")

        val createdAt = updatedUser?.createdAt?.truncatedTo(SECONDS)
        val updatedAt = updatedUser?.updatedAt?.truncatedTo(SECONDS)

        assertThat(Duration.between(createdAt, updatedAt)).isEqualTo(Duration.ofSeconds(3))
    }
}
