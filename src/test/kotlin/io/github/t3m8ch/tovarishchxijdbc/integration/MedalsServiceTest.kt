package io.github.t3m8ch.tovarishchxijdbc.integration

import io.github.t3m8ch.tovarishchxijdbc.ContextInitializer
import io.github.t3m8ch.tovarishchxijdbc.TestWithoutTelegramBot
import io.github.t3m8ch.tovarishchxijdbc.builders.userModels
import io.github.t3m8ch.tovarishchxijdbc.data.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisTelegramIdNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.exceptions.UserWithThisUserNameNotFoundException
import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
@ContextConfiguration(initializers = [ContextInitializer::class])
class MedalsServiceTest(
    @Autowired private val medalsService: MedalsService,
    @Autowired private val usersRepository: UsersRepository,
) : TestWithoutTelegramBot() {
    @BeforeEach
    fun beforeEach() {
        usersRepository.deleteAll()
    }

    @Test
    fun `test give medals by telegramId`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasMedals(5)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasMedals(5)
            }
        }
        usersRepository.saveAll(users)

        // Act
        val actualNewMedalsCount = medalsService.giveMedalsByTelegramId(count = 5, targetTelegramId = 1)

        // Assert
        assertThat(actualNewMedalsCount).isEqualTo(10)

        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        assertThat(updatedUser?.medalsCount).isEqualTo(10)
        assertThat(notUpdatedUser?.medalsCount).isEqualTo(5)
    }

    @Test
    fun `test give medals by userName`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasMedals(5)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasMedals(5)
            }
        }
        usersRepository.saveAll(users)

        // Act
        val actualNewMedalsCount = medalsService.giveMedalsByUserName(count = 5, targetUserName = "petya")

        // Assert
        assertThat(actualNewMedalsCount).isEqualTo(10)

        val updatedUser = usersRepository.findOrNullByTelegramId(1)
        val notUpdatedUser = usersRepository.findOrNullByTelegramId(2)

        assertThat(updatedUser?.medalsCount).isEqualTo(10)
        assertThat(notUpdatedUser?.medalsCount).isEqualTo(5)
    }

    @Test
    fun `test give medals by telegramId, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasTelegramId(1)
                hasMedals(5)
            }
            that {
                hasCount(1)
                hasTelegramId(2)
                hasMedals(5)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        assertThatThrownBy {
            medalsService.giveMedalsByTelegramId(count = 5, targetTelegramId = 3)
        }.isInstanceOf(UserWithThisTelegramIdNotFoundException::class.java)
    }

    @Test
    fun `test give medals by userName, if user not found`() {
        // Arrange
        val users = userModels {
            that {
                hasCount(1)
                hasUsername("petya")
                hasMedals(5)
            }
            that {
                hasCount(1)
                hasUsername("vasya")
                hasMedals(5)
            }
        }
        usersRepository.saveAll(users)

        // Act & Assert
        assertThatThrownBy {
            medalsService.giveMedalsByUserName(count = 5, targetUserName = "Sanya")
        }.isInstanceOf(UserWithThisUserNameNotFoundException::class.java)
    }
}
