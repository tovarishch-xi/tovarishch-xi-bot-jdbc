package io.github.t3m8ch.tovarishchxijdbc.builders

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel

fun userModels(configure: UserModelsBuilder.() -> Unit): List<UserModel> {
    val builder = UserModelsBuilder()
    builder.configure()
    return builder.build()
}

class UserModelsBuilder : TestDataBuilder<UserModelThatElement, UserModel>({
    createThatElement = { UserModelThatElement() }
    mapThatElementToObject = { it, i ->
        UserModel(
            telegramId = it.telegramId ?: i,
            firstName = it.firstName,
            lastName = it.lastName,
            userName = it.userName,
            medalsCount = it.medalsCount,
            warnsCount = it.warnsCount,
        )
    }
})

class UserModelThatElement : ThatElement() {
    var telegramId: Long? = null
        private set

    var firstName: String = ""
        private set

    var lastName: String? = null
        private set

    var userName: String? = null
        private set

    var medalsCount: Int = 0
        private set

    var warnsCount: Int = 0
        private set

    fun hasTelegramId(telegramId: Long) {
        this.telegramId = telegramId
    }

    fun hasFirstName(firstName: String) {
        this.firstName = firstName
    }

    fun hasLastName(lastName: String) {
        this.lastName = lastName
    }

    fun hasUsername(userName: String) {
        this.userName = userName
    }

    fun hasMedals(count: Int) {
        this.medalsCount = count
    }

    fun hasWarns(count: Int) {
        this.warnsCount = count
    }
}
