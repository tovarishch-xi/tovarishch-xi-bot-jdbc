package io.github.t3m8ch.tovarishchxijdbc.factories

import io.github.t3m8ch.tovarishchxijdbc.data.models.UserModel
import io.github.t3m8ch.tovarishchxijdbc.telegram.Context
import org.mockito.Mockito.mock
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.bots.AbsSender

fun createMockContext(update: Update) = Context(
    update = update,
    sender = mock(AbsSender::class.java),
    user = UserModel(firstName = "", telegramId = 1)
)
