package io.github.t3m8ch.tovarishchxijdbc.factories

import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.User

fun createMockUpdateWithMessage(
    messageId: Int = 10,
    messageText: String?,
    chatType: String,
    chatId: Long = 1,
    replyToId: Long? = null,
    replyToBot: Boolean = false,
    fromId: Long = 10,
    fromUsername: String? = null,
): Update {
    val update = Update()
    update.updateId = 1
    val message = Message()
    message.text = messageText
    update.message = message
    message.messageId = messageId
    message.chat = Chat()
    message.chat.type = chatType
    message.chat.id = chatId
    message.replyToMessage = replyToId?.let {
        val repliedMessage = Message()
        repliedMessage.messageId = 1
        repliedMessage.from = User()
        repliedMessage.from.id = replyToId
        repliedMessage.from.isBot = replyToBot
        repliedMessage
    }
    message.from = User()
    message.from.id = fromId
    message.from.userName = fromUsername

    return update
}

fun createMockUpdateWithoutMessage(): Update {
    val update = Update()
    update.updateId = 1
    update.message = null
    return update
}
