package io.github.t3m8ch.tovarishchxijdbc.factories

import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.GiveMedalsCommandHandler
import org.junit.jupiter.api.DynamicTest
import org.mockito.Mockito

class DynamicTestFactoryForSuccessfullyGiveMedalsCommandHandlingTests {
    companion object {
        data class Input(
            val messageText: String,
            val replyTo: Long? = null,
        )

        data class Output(
            val medalsCount: Int,
            val targetUserName: String? = null,
            val targetTelegramId: Long? = null,
        )

        fun create(input: Input, output: Output): DynamicTest =
            DynamicTest.dynamicTest("handler should handle input: $input and return $output") {
                val context = createMockContext(
                    createMockUpdateWithMessage(
                        messageText = input.messageText,
                        chatType = "group",
                        replyToId = input.replyTo,
                    )
                )
                val medalsService = Mockito.mock(MedalsService::class.java)
                val handler = GiveMedalsCommandHandler(medalsService)
                handler.handle(context)

                if (output.targetUserName != null) {
                    Mockito.verify(medalsService).giveMedalsByUserName(output.medalsCount, output.targetUserName)
                    Mockito.verify(medalsService, Mockito.times(0))
                        .giveMedalsByTelegramId(Mockito.anyInt(), Mockito.anyLong())
                } else if (output.targetTelegramId != null) {
                    Mockito.verify(medalsService, Mockito.times(0))
                        .giveMedalsByUserName(Mockito.anyInt(), Mockito.anyString())
                    Mockito.verify(medalsService).giveMedalsByTelegramId(output.medalsCount, output.targetTelegramId)
                }
            }
    }
}
