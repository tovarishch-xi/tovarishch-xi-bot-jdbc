package io.github.t3m8ch.tovarishchxijdbc.unit

import io.github.t3m8ch.tovarishchxijdbc.factories.createMockContext
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithMessage
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithoutMessage
import io.github.t3m8ch.tovarishchxijdbc.services.RefreshService
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.RefreshCommandHandler
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMemberAdministrator

class RefreshCommandHandlerTest {
    @ParameterizedTest
    @ValueSource(strings = ["group", "supergroup"])
    fun `filter should return true`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "!refresh", chatType = chatType)
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = RefreshCommandHandler(Mockito.mock(RefreshService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isTrue
    }

    @ParameterizedTest
    @ValueSource(strings = ["private", "channel"])
    fun `filter should return false with this chat types`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "!refresh", chatType = chatType)
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = RefreshCommandHandler(Mockito.mock(RefreshService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isFalse
    }

    @Test
    fun `filter should return false, if there are no message in update`() {
        // Arrange
        val update = createMockUpdateWithoutMessage()
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = RefreshCommandHandler(Mockito.mock(RefreshService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isFalse
    }

    @Test
    fun `handler should refresh, if there are reply`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!refresh",
                chatType = "group",
                chatId = 10,
                replyToId = 1,
            )
        )
        val refreshService = Mockito.mock(RefreshService::class.java)
        val handler = RefreshCommandHandler(refreshService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(refreshService).refreshByTelegramId(1)
        Mockito.verify(refreshService, Mockito.times(0)).refreshByUserName(ArgumentMatchers.anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text("Данные пользователя обнулены")
                .build()
        )
    }

    @Test
    fun `handler should refresh, if userName is specified`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!refresh @vasya",
                chatType = "group",
                chatId = 10,
            )
        )
        val refreshService = Mockito.mock(RefreshService::class.java)
        val handler = RefreshCommandHandler(refreshService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(refreshService, Mockito.times(0)).refreshByTelegramId(ArgumentMatchers.anyLong())
        Mockito.verify(refreshService).refreshByUserName("vasya")
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text("Данные пользователя обнулены")
                .build()
        )
    }

    @Test
    fun `handler should not refresh, if userName is not specified or there is no reply`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!refresh",
                chatType = "group",
                chatId = 10,
            )
        )
        val refreshService = Mockito.mock(RefreshService::class.java)
        val handler = RefreshCommandHandler(refreshService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(refreshService, Mockito.times(0)).refreshByTelegramId(ArgumentMatchers.anyLong())
        Mockito.verify(refreshService, Mockito.times(0)).refreshByUserName(ArgumentMatchers.anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!refresh @vasya2005</code>
                    """.trimIndent()
                )
                .parseMode("HTML")
                .build()
        )
    }

    @Test
    fun `handler should not refresh, if text after !refresh does not start with @`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!refresh fdfd",
                chatType = "group",
                chatId = 10,
            )
        )
        val refreshService = Mockito.mock(RefreshService::class.java)
        val handler = RefreshCommandHandler(refreshService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(refreshService, Mockito.times(0)).refreshByTelegramId(ArgumentMatchers.anyLong())
        Mockito.verify(refreshService, Mockito.times(0)).refreshByUserName(ArgumentMatchers.anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!refresh @vasya2005</code>
                    """.trimIndent()
                )
                .parseMode("HTML")
                .build()
        )
    }

    @Test
    fun `handler should not refresh, if user reply to bot`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!refresh",
                replyToId = 20,
                chatType = "group",
                replyToBot = true,
                chatId = 10,
            )
        )
        val refreshService = Mockito.mock(RefreshService::class.java)
        val handler = RefreshCommandHandler(refreshService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(refreshService, Mockito.times(0)).refreshByTelegramId(ArgumentMatchers.anyLong())
        Mockito.verify(refreshService, Mockito.times(0)).refreshByUserName(ArgumentMatchers.anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text("Вы не можете обнулить медали и предупреждения бота")
                .build()
        )
    }
}
