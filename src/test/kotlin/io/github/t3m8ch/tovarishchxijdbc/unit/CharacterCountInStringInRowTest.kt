package io.github.t3m8ch.tovarishchxijdbc.unit

import io.github.t3m8ch.tovarishchxijdbc.telegram.utils.getCharacterCountInStringInRow
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CharacterCountInStringInRowTest {
    @Test
    fun `test, if there are no pluses in string`() {
        assertThat("Hello, world".getCharacterCountInStringInRow('+')).isEqualTo(0)
    }

    @Test
    fun `test, if there is one plus in string`() {
        assertThat("+ @username".getCharacterCountInStringInRow('+')).isEqualTo(1)
    }

    @Test
    fun `test, if there are three pluses in a row`() {
        assertThat("+++ @username".getCharacterCountInStringInRow('+')).isEqualTo(3)
    }

    @Test
    fun `test, if there are five pluses not in a row`() {
        assertThat("++ 5 + 3 + 2 = 8 + 2".getCharacterCountInStringInRow('+')).isEqualTo(2)
    }
}
