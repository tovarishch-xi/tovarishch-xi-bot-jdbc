package io.github.t3m8ch.tovarishchxijdbc.unit

import io.github.t3m8ch.tovarishchxijdbc.factories.DynamicTestFactoryForSuccessfullyGiveMedalsCommandHandlingTests
import io.github.t3m8ch.tovarishchxijdbc.factories.DynamicTestFactoryForSuccessfullyGiveMedalsCommandHandlingTests.Companion.Input
import io.github.t3m8ch.tovarishchxijdbc.factories.DynamicTestFactoryForSuccessfullyGiveMedalsCommandHandlingTests.Companion.Output
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockContext
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithMessage
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithoutMessage
import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.GiveMedalsCommandHandler
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.NullSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.chatmember.*
import java.util.stream.Stream

class GiveMedalsCommandHandlerTest {
    @ParameterizedTest
    @ValueSource(strings = ["group", "supergroup"])
    fun `filter should return true`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "+++ Cool", chatType = chatType)
        val context = createMockContext(update)
        `when`(context.sender.execute(any(GetChatMember::class.java))).thenReturn(ChatMemberAdministrator())

        val handler = GiveMedalsCommandHandler(mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        assertThat(success).isTrue
    }

    @ParameterizedTest
    @ValueSource(strings = ["private", "channel"])
    fun `filter should return false with this chat types`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "+++ Cool", chatType = chatType)
        val context = createMockContext(update)
        `when`(context.sender.execute(any(GetChatMember::class.java))).thenReturn(ChatMemberAdministrator())

        val handler = GiveMedalsCommandHandler(mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        assertThat(success).isFalse
    }

    @Test
    fun `filter should return false, if there are no message in update`() {
        // Arrange
        val update = createMockUpdateWithoutMessage()
        val context = createMockContext(update)
        `when`(context.sender.execute(any(GetChatMember::class.java))).thenReturn(ChatMemberAdministrator())

        val handler = GiveMedalsCommandHandler(mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        assertThat(success).isFalse
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = ["Something"])
    fun `filter should return false with this message texts`(messageText: String?) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = messageText, chatType = "group")
        val context = createMockContext(update)
        `when`(context.sender.execute(any(GetChatMember::class.java))).thenReturn(ChatMemberAdministrator())

        val handler = GiveMedalsCommandHandler(mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        assertThat(success).isFalse
    }

    @ParameterizedTest
    @MethodSource("provideChatMembersWhoHaveNoRights")
    fun `filter should return false, if user is not admin or owner of chat`(chatMember: ChatMember) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "+++ Cool", chatType = "group")
        val context = createMockContext(update)
        `when`(context.sender.execute(any(GetChatMember::class.java))).thenReturn(chatMember)

        val handler = GiveMedalsCommandHandler(mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        assertThat(success).isFalse
    }

    @TestFactory
    fun `handler must successfully handle update`(): Collection<DynamicTest> {
        val inputOutputMap = mapOf(
            Input(messageText = "+++ Cool", replyTo = 1) to Output(medalsCount = 3, targetTelegramId = 1),
            Input(messageText = "++++++++++ Cool", replyTo = 1) to Output(medalsCount = 5, targetTelegramId = 1),
            Input(messageText = "+++ @coolvasya") to Output(medalsCount = 3, targetUserName = "coolvasya"),
            Input(messageText = "+++ @coolvasya", replyTo = 1) to Output(medalsCount = 3, targetUserName = "coolvasya"),
        )

        return inputOutputMap.keys.zip(inputOutputMap.values).map { (input, output) ->
            DynamicTestFactoryForSuccessfullyGiveMedalsCommandHandlingTests.create(input, output)
        }
    }

    @Test
    fun `handler should not increase medals count for bot`() {
        // Arrange
        val update = createMockUpdateWithMessage(
            messageId = 10,
            messageText = "+++",
            chatType = "group",
            replyToId = 0,
            replyToBot = true,
        )
        val context = createMockContext(update)
        val medalsService = mock(MedalsService::class.java)
        val handler = GiveMedalsCommandHandler(medalsService)

        // Act
        handler.handle(context)

        // Arrange
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByTelegramId(Mockito.anyInt(), Mockito.anyLong())
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByUserName(Mockito.anyInt(), Mockito.anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(10)
                .chatId(1)
                .text("Вы не можете выдавать медальки боту")
                .build()
        )
    }

    @Test
    fun `handler should not respond to message with plus, but without reply or specifying userName`() {
        // Arrange
        val update = createMockUpdateWithMessage(
            messageText = "+++ Cool",
            chatType = "group",
            replyToId = null,
        )
        val context = createMockContext(update)
        val medalsService = mock(MedalsService::class.java)
        val handler = GiveMedalsCommandHandler(medalsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByTelegramId(Mockito.anyInt(), Mockito.anyLong())
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByUserName(Mockito.anyInt(), Mockito.anyString())
        Mockito.verify(context.sender, Mockito.times(0)).execute(any(SendMessage::class.java))
    }

    @Test
    fun `handler should not respond to message that is response from user to himself`() {
        // Arrange
        val update = createMockUpdateWithMessage(
            messageText = "+++ Cool",
            chatType = "group",
            replyToId = 1,
            fromId = 1,
        )
        val context = createMockContext(update)
        val medalsService = mock(MedalsService::class.java)
        val handler = GiveMedalsCommandHandler(medalsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByTelegramId(Mockito.anyInt(), Mockito.anyLong())
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByUserName(Mockito.anyInt(), Mockito.anyString())
        Mockito.verify(context.sender, Mockito.times(0)).execute(any(SendMessage::class.java))
    }

    @Test
    fun `handler should not respond to message containing sender's userName`() { // Arrange
        val update = createMockUpdateWithMessage(
            messageText = "+++ @t3m8ch",
            chatType = "group",
            replyToId = 1,
            fromUsername = "t3m8ch"
        )
        val context = createMockContext(update)
        val medalsService = mock(MedalsService::class.java)
        val handler = GiveMedalsCommandHandler(medalsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByTelegramId(Mockito.anyInt(), Mockito.anyLong())
        Mockito.verify(medalsService, Mockito.times(0)).giveMedalsByUserName(Mockito.anyInt(), Mockito.anyString())
        Mockito.verify(context.sender, Mockito.times(0)).execute(any(SendMessage::class.java))
    }

    companion object {
        @JvmStatic
        private fun provideChatMembersWhoHaveNoRights() = Stream.of(
            Arguments.of(ChatMemberMember()),
            Arguments.of(ChatMemberBanned()),
            Arguments.of(ChatMemberLeft()),
            Arguments.of(ChatMemberRestricted()),
        )
    }
}
