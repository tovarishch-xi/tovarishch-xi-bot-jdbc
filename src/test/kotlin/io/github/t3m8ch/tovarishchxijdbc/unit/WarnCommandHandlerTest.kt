package io.github.t3m8ch.tovarishchxijdbc.unit

import io.github.t3m8ch.tovarishchxijdbc.factories.createMockContext
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithMessage
import io.github.t3m8ch.tovarishchxijdbc.factories.createMockUpdateWithoutMessage
import io.github.t3m8ch.tovarishchxijdbc.services.MedalsService
import io.github.t3m8ch.tovarishchxijdbc.services.WarnsService
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.GiveMedalsCommandHandler
import io.github.t3m8ch.tovarishchxijdbc.telegram.handlers.WarnCommandHandler
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMemberAdministrator

class WarnCommandHandlerTest {
    @ParameterizedTest
    @ValueSource(strings = ["group", "supergroup"])
    fun `filter should return true`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "!warn", chatType = chatType)
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = WarnCommandHandler(Mockito.mock(WarnsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isTrue
    }

    @ParameterizedTest
    @ValueSource(strings = ["private", "channel"])
    fun `filter should return false with this chat types`(chatType: String) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = "!warn", chatType = chatType)
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = WarnCommandHandler(Mockito.mock(WarnsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isFalse
    }

    @Test
    fun `filter should return false, if there are no message in update`() {
        // Arrange
        val update = createMockUpdateWithoutMessage()
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = WarnCommandHandler(Mockito.mock(WarnsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isFalse
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = ["Something"])
    fun `filter should return false with this message texts`(messageText: String?) {
        // Arrange
        val update = createMockUpdateWithMessage(messageText = messageText, chatType = "group")
        val context = createMockContext(update)
        Mockito.`when`(context.sender.execute(ArgumentMatchers.any(GetChatMember::class.java)))
            .thenReturn(ChatMemberAdministrator())

        val handler = GiveMedalsCommandHandler(Mockito.mock(MedalsService::class.java))

        // Act
        val success = handler.filter(context)

        // Assert
        Assertions.assertThat(success).isFalse
    }

    @Test
    fun `handler should give warn, if there are reply`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!warn",
                chatType = "group",
                chatId = 10,
                replyToId = 1,
            )
        )
        val warnsService = Mockito.mock(WarnsService::class.java)
        Mockito.`when`(warnsService.warnByTelegramId(1)).thenReturn(5)
        val handler = WarnCommandHandler(warnsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(warnsService).warnByTelegramId(1)
        Mockito.verify(warnsService, Mockito.times(0)).warnByUserName(anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы выдали предупреждение пользователю.
                        Теперь у этого пользователя 5 предупреждений
                    """.trimIndent()
                )
                .build()
        )
    }

    @Test
    fun `handler should give warn, if nickname is specified`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!warn @vasya",
                chatType = "group",
                chatId = 10,
            )
        )
        val warnsService = Mockito.mock(WarnsService::class.java)
        Mockito.`when`(warnsService.warnByUserName("vasya")).thenReturn(5)
        val handler = WarnCommandHandler(warnsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(warnsService, Mockito.times(0)).warnByTelegramId(anyLong())
        Mockito.verify(warnsService).warnByUserName("vasya")
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы выдали предупреждение пользователю.
                        Теперь у этого пользователя 5 предупреждений
                    """.trimIndent()
                )
                .build()
        )
    }

    @Test
    fun `handler should not give warn, if nick is not specified or there is no reply`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!warn",
                chatType = "group",
                chatId = 10,
            )
        )
        val warnsService = Mockito.mock(WarnsService::class.java)
        Mockito.`when`(warnsService.warnByUserName("vasya")).thenReturn(5)
        val handler = WarnCommandHandler(warnsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(warnsService, Mockito.times(0)).warnByTelegramId(anyLong())
        Mockito.verify(warnsService, Mockito.times(0)).warnByUserName(anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!warn @vasya2005</code>
                    """.trimIndent()
                )
                .parseMode("HTML")
                .build()
        )
    }

    @Test
    fun `handler should not give warn, if text after !warn does not start with @`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!warn fdfd",
                chatType = "group",
                chatId = 10,
            )
        )
        val warnsService = Mockito.mock(WarnsService::class.java)
        Mockito.`when`(warnsService.warnByUserName("vasya")).thenReturn(5)
        val handler = WarnCommandHandler(warnsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(warnsService, Mockito.times(0)).warnByTelegramId(anyLong())
        Mockito.verify(warnsService, Mockito.times(0)).warnByUserName(anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text(
                    """
                        Вы должны ответить на сообщение пользователя, которому вы хотите выдать предупреждение.
                        
                        Также вместо ответа на сообщение вы можете указать имя пользователя. Например, <code>!warn @vasya2005</code>
                    """.trimIndent()
                )
                .parseMode("HTML")
                .build()
        )
    }

    @Test
    fun `handler should not give warn, if user reply to bot`() {
        // Arrange
        val context = createMockContext(
            createMockUpdateWithMessage(
                messageId = 11,
                messageText = "!warn fdfd",
                replyToId = 20,
                chatType = "group",
                replyToBot = true,
                chatId = 10,
            )
        )
        val warnsService = Mockito.mock(WarnsService::class.java)
        Mockito.`when`(warnsService.warnByUserName("vasya")).thenReturn(5)
        val handler = WarnCommandHandler(warnsService)

        // Act
        handler.handle(context)

        // Assert
        Mockito.verify(warnsService, Mockito.times(0)).warnByTelegramId(anyLong())
        Mockito.verify(warnsService, Mockito.times(0)).warnByUserName(anyString())
        Mockito.verify(context.sender).execute(
            SendMessage.builder()
                .replyToMessageId(11)
                .chatId(10)
                .text("Вы не можете выдавать предупреждения боту")
                .build()
        )
    }
}
