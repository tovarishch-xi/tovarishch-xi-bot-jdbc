FROM gradle:7.5-jdk17-alpine AS build

RUN mkdir /building
COPY . /building
WORKDIR /building
RUN gradle build --exclude-task test --no-daemon --info

FROM openjdk:17-alpine

RUN mkdir /libs
COPY --from=build /building/build/libs/ /libs/
ENTRYPOINT ["java", "-jar", "/libs/tovarishch-xi-jdbc-0.0.1-SNAPSHOT.jar"]
